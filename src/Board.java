
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 *
 * @author ondrejtybl
 * 
 * Třída obsluhující GUI. Voláno z konstruktoru Game.
 * Tato třída neví, zda je hrána síťová hra nebo ne,
 * slouží pouze pro komunikaci s uživatelem.
 * 
 */

public class Board extends JFrame {

    JPanel field = new JPanel();
    JPanel menu = new JPanel();
    JButton[][] buttons = new JButton[10][10];
    JTextField turnInfo = new JTextField();
    JMenuItem newGame, helpButton, exit, hostNewGame, joinNewGame;

    public Board(String title) {
        super("TicTacToe "+title);
        setSize(800, 800);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        setMenu();
        setField();
        setVisible(true);
    }
    
    /*
     * Inicializace menu a výstupní řádky
     * pro komunikaci s uživatelem.
     * Listenery dodává třída Game, protože je
     * nutné vědět, jestli jde o síťovou hru.
     */

    private void setMenu() {
        JMenuBar m = new JMenuBar();
        setJMenuBar(m);
        JMenu file = new JMenu("File");
        JMenu help = new JMenu("Help");
        m.add(file);
        m.add(help);
        newGame = new JMenuItem("New Game");
        file.add(newGame);        
        hostNewGame = new JMenuItem("Host new game");
        file.add(hostNewGame);
        joinNewGame = new JMenuItem("Join new game");
        file.add(joinNewGame);
        exit = new JMenuItem("Exit");
        file.add(exit);
        helpButton = new JMenuItem("Show Help");
        help.add(helpButton);
        add(menu, BorderLayout.NORTH);
        add(turnInfo, BorderLayout.AFTER_LAST_LINE);
        turnInfo.setEditable(false);
        turnInfo.setFont(new Font("SansSerif", Font.PLAIN, 20));
        turnInfo.setText("Player #1 on turn. Round #1");
    }
    
    /*
     * Inicializace políček pro samotnou hru.
     * Listenery dodává třída Game, protože je
     * nutné vědět, jestli jde o síťovou hru.
     */

    private void setField() {
        add(field, BorderLayout.CENTER);
        field.setLayout(new GridLayout(11, 11));
        for (int i = 0; i < 11; i++) {
            for (int j = 0; j < 11; j++) {
                if (i == 10 & j == 0) {
                    JButton corner = new JButton();
                    corner.setEnabled(false);
                    field.add(corner);
                } else if (j == 0) {
                    JButton around = new JButton("" + (char) ('A' + i));
                    around.setEnabled(false);
                    field.add(around);
                } else if (i == 10) {
                    JButton around = new JButton("" + (char) ('A' + j - 1));
                    around.setEnabled(false);
                    field.add(around);
                } else {
                    buttons[i][j - 1] = new JButton();
                    buttons[i][j - 1].setSize(4, 4);
                    field.add(buttons[i][j - 1]);
                }
            }
        }
    }
}
