
import javax.swing.JOptionPane;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ondrejtybl
 */
public class Start {

    /**
     * @param args the command line arguments
     * 
     * Spouštecí metoda programu.
     * Na panelu uživatel vybere, zda chce síťovou hru (buď připojit již existující nebo vytvořit novou),
     * nebo jestli chce pouze offline hru. Poté vytvoří instance třídy Game příslušným konstruktorem.
     * Pro síťovou hru se vždy vytvoří objekt Server, který se chová buď jako server (vytvoří
     * serverSocket) nebo jako klient. Následně spustí vlákno zajišťující vzdálenou komunikaci.
     * 
     */
    public static void main(String[] args) {
        Game game;

        String[] options = new String[]{"Offline", "Host", "Join"};
        int response = JOptionPane.showOptionDialog(null, "Select one:", "Welcome to TicTacToe",
                JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE,
                null, options, options[0]);
        switch (response) {
            case 0: game = new Game();
                break;
            case 1: game = new Game(true);
                break;
            default: game = new Game(false);
        }

    }
}
