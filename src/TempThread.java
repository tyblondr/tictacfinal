/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.*;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author ondrejtybl
 * 
 * Dočasné vlákno, které zajistí propojení s protihráčem
 * a spustí další vlákno, které v průběhu celé hry zajišťuje
 * přenos informací.
 * 
 */
public class TempThread implements Runnable {

    Server server;
    Thread tmp;

    public TempThread(Server server) {
        this.server = server;
    }
    
    /*
     * Metoda zprovozňující síťové připojení. Pokud hráč
     * zakládá novou hru, vytvoří serverSocket. V každém případě
     * vytvoří socket a čeká, dokud se propojí se serverSocketem.
     * Umožňuje uživateli zadat adresu, na kterou se chce připojit.
     * Nakonec spustí vlákno zajišťující přenos informací.
     */

    @Override
    public void run() {
        try {
            if (server.isServer) {
                ServerSocket serverSocket = new ServerSocket(4091);
                server.socket = serverSocket.accept();
                server.ois = new ObjectInputStream(server.socket.getInputStream());
                server.oos = new ObjectOutputStream(server.socket.getOutputStream());
            } else {
                String host = JOptionPane.showInputDialog("Type IP idress", "\"localhost\"");
                String portS = JOptionPane.showInputDialog("Type port number", 4091);
                int portI = Integer.parseInt(portS);
                while (true) {
                    try {
                        server.socket = new Socket("localhost", portI);
                        if (server.socket != null) {
                            server.oos = new ObjectOutputStream(server.socket.getOutputStream());
                            server.ois = new ObjectInputStream(server.socket.getInputStream());
                            break;
                        }
                    } catch (IOException e) {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException ex) {
                            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
        server.game.board.turnInfo.setText("Connencted. Player #1 on turn. Round #1");
        server.game.turn = server.isServer;
        server.comm = new Communicator(server);
        server.comm.start();
    }

    public void start() {
        tmp = new Thread(this);
        tmp.start();
    }
}
