/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ondrejtybl
 * 
 * Konstanty určující hráče fakt, že políčko nebylo dosud označeno.
 * Pro snadnější práci v actionListenerech při zaškrtávání políček a určování vítěze
 * 
 */

public enum Value {
    
    FIRST(1), SECOND(2), NONE(0);
    
    private final int num;
    
    private Value(int num) {
        this.num = num;
    }
    
    public int getNum() {
        return this.num;
    }
}