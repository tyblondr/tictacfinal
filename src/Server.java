/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.*;
import java.io.*;
/**
 *
 * @author ondrejtybl
 *
 * Třída zprovozňující síťovou hru. Obsahuje objekt Game, aby mohla využívat
 * jeho metody a komunikovat s Board. Spustí nové vlákno, které zajistí připojení,
 * aby mohl uživatel uvládat GUI bez ohledu na proces připojování.
 */
public class Server {

    boolean isServer;
    Game game;
    Socket socket;
    ObjectInputStream ois;
    ObjectOutputStream oos;
    Communicator comm;
    TempThread tmp;

    /*
     * Konstruktor, který vyžaduje informace,
     * zda hráč vytváří novou hru, nebo se pouze
     * připojuje.
     */
    public Server(boolean isServer, Game game) {
        game.board.turnInfo.setText("Waiting for connections...");
        this.isServer = isServer;
        this.game = game;
        game.turn = false;
        tmp = new TempThread(this);
        tmp.start();
    }

    /*
     * Metoda, kterou volá vlákno Communicator,
     * pokud je mu nařízeno, aby skončilo. Tato metoda
     * pak zařídí, aby bylo vše anulováno pro začátek hry nové.
     */
    public synchronized void endOfOnlineGame() {
        for (int i = 0; i < game.list.length; i++) {
            for (int j = 0; j < game.list[i].length; j++) {
                game.list[i][j] = Value.NONE;
                if (game.board.buttons[i][j].getActionListeners().length != 0) {
                    game.board.buttons[i][j].removeActionListener(game.board.buttons[i][j].getActionListeners()[0]);
                }
            }
        }
        game.turn = isServer;
        game.round = 1;
        game.setListeners();
        comm.sendFirst = isServer;
        comm.start();
    }
}