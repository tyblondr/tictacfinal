
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ondrejtybl
 * 
 * Zajišťuje samotnou vzdálenou komunikaci. Vlákno
 * spuštěné třídou Server, která se sama tomuto vláknu
 * předá, aby bylo možno odsud obsluhovat metody Game.
 */
public class Communicator implements Runnable {

    Server server;
    ObjToSend temp;
    boolean sendFirst;
    Thread communication = null;
    
    /*
     * Konstruktor předávájící Server,
     * přes který se tato třída dostane
     * ke Game.
     */

    public Communicator(Server server) {
        this.sendFirst = server.isServer;
        this.server = server;
    }
    
    /*
     * Metoda, kteoru se vlákno nastartuje.
     * (odlišné od metody run() pro konzistentní
     * chování.
     */

    public void start() {
        communication = new Thread(this);
        communication.start();
    }
    
    /*
     * Samotná komunikace. Cyklus se opakuje dokud
     * není ve třídě Game vyhodnoceno, že je konec hry. Proměnnná sendFirst
     * rozhoduje, zda vlákno začíná v pozici čekání na odeslání nebo čekání
     * na přijmutí. Čekání na přijmutí je opět cyklus. Pokud metoda zjistí,
     * že objToSend ve třídě Game již byl naplněn informacemi, pošle ho a následně
     * zase vynuluje. Poté se dostane do cyklu, kde čeká na přijetí dat, která
     * pak odevzdá metodě performTurn() ve třídě Game. Pokud bylo vyhodnoceno skončení hry,
     * cylkus se ukončí a provede se anulovaní všech hodnot pro novou hru a spuštění
     * nového vlákna.
     */

    @Override
    public synchronized void run() {
        server.game.isEnd = false;
        while (!server.game.isEnd) {
            if (sendFirst) {
                while (!server.game.isEnd) {
                    try {
                        wait(10);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Communicator.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    if (server.game.getObjToSend() != null) {
                        try {
                            server.oos.writeObject(server.game.getObjToSend());
                            server.game.objToSend = null;
                            break;
                        } catch (IOException ex) {
                            Logger.getLogger(Communicator.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }
            while (!server.game.isEnd) {
                sendFirst = true;
                try {
                    temp = (ObjToSend) server.ois.readObject();
                    if (temp != null) {
                        if (!server.isServer) {
                            server.game.performTurn(temp.x, temp.y, Value.FIRST, false);
                        } else {
                            server.game.performTurn(temp.x, temp.y, Value.SECOND, false);
                        }
                        temp = null;
                        break;
                    }
                } catch (IOException | ClassNotFoundException ex) {
                    JOptionPane.showMessageDialog(null, "Error. Your opponent has probably quit the game.");
                    System.exit(0);
                }
            }
        }
        server.endOfOnlineGame();
    }
}
