
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/**
 *
 * @author ondrejtybl
 *
 * Hlavní třída celého programu. Zajišťuje vyhodnocování hráčova tahu. Při
 * síťové hře vytváří Server, kterému předá sama sebe. Obsahuje matici objektů
 * Value pro vyhodnocování vítězství. Ke Game mají jednoduchý přístup všechny
 * listenery jako privátní třídy.
 *
 */
public class Game {

    Board board;
    Value[][] list = new Value[10][10];
    boolean turn;
    boolean isServer;
    boolean isOnline;
    boolean isEnd = false;
    int round = 1;
    ObjToSend objToSend;
    ImageIcon X = new ImageIcon(this.getClass().getResource("x.png"));
    ImageIcon O = new ImageIcon(this.getClass().getResource("o.png"));

    /*
     * Konstruktor volaný pro nesíťovou hru.
     */
    public Game() {
        for (int i = 0; i < list.length; i++) {
            for (int j = 0; j < list[i].length; j++) {
                list[i][j] = Value.NONE;
            }
        }
        board = new Board("Offline");
        setListeners();
        turn = true;
    }

    /*
     * Konstruktor volaný pro síťovou hru.
     * Vytvoří objekt Board a Server, kterému předá sám sebe.
     * Hra se tak rozdvojuje - vstup jak z rozhraní, 
     * které zajišťuje Board, tak ze síťové
     * komunikace pomocí Serveru.
     */
    public Game(boolean isServer) {
        for (int i = 0; i < list.length; i++) {
            for (int j = 0; j < list[i].length; j++) {
                list[i][j] = Value.NONE;
            }
        }
        isOnline = true;
        if (isServer) {
            board = new Board("Player #1");
        } else {
            board = new Board("Player #2");
        }
        this.isServer = isServer;
        Server server = new Server(isServer, this);
        setListeners();
        turn = isServer;
    }

    /*
     * Metoda používaná pouze pro offline hru.
     * Po výhře jednoho z hráčů se zavolá, 
     * aby znovu inicializovala všechna políčka
     * a nastavila kolo a tah.
     */
    public void offlineAnnul() {
        for (int i = 0; i < list.length; i++) {
            for (int j = 0; j < list[i].length; j++) {
                list[i][j] = Value.NONE;
                if (board.buttons[i][j].getActionListeners().length != 0) {
                    board.buttons[i][j].removeActionListener(board.buttons[i][j].getActionListeners()[0]);
                }
            }
        }
        turn = true;
        round = 1;
        setListeners();
    }

    /*
     * Synchronizovaná metoda, kterou volá jak uživatel
     * stisknutím políčka ve svém GUI, tak vlákno
     * Communicator zadávající externí informace.
     * Při volání z actionListeneru je setObj == true
     * a objekt objToSend je nastaven tak, aby mohl
     * být vlkáknem Communicator odeslán. V příslušném
     * políčku se odstraní actionListener, obarví se políčko a zavolá se metoda
     * pro vyhodnocení případné výhry. Pokud je test pozitivní,
     * předá se informace vláknu Communicator, že má skončit, aby
     * mohla být bezpečně započata nová hra.
     */
    public synchronized void performTurn(int x, int y, Value value, boolean setObj) {
        turn = !turn;
        if (board.buttons[x][y].getActionListeners().length != 0) {
            board.buttons[x][y].removeActionListener(board.buttons[x][y].getActionListeners()[0]);
        }
        if (value == Value.FIRST) {
            board.buttons[x][y].setIcon(X);
        } else {
            board.buttons[x][y].setIcon(O);
        }
        list[x][y] = value;
        round++;
        board.turnInfo.setText(getTurnToString(x, y, value));
        if (checkWin(x, y) != 0) {
            board.turnInfo.setText("Player #" + list[x][y].getNum() + " won. Let's play another one. Now player #1 starts");
            if (!isOnline) {
                offlineAnnul();
            }
            if (setObj) {
                objToSend = new ObjToSend(x, y);
            }
            isEnd = true;
        }
        if (setObj) {
            objToSend = new ObjToSend(x, y);
        }
    }

    /*
     * Vrací prvek matice hodnot políček.
     */
    public Value getValue(int x, int y) {
        return list[x][y];
    }

    /*
     * Pro offline hru určuje barvu hráč, který je na řadě.
     * Pro online hru zabezpečuje, aby hráč hrál pouze když je na řadě.
     */
    public boolean getTurn() {
        return turn;
    }

    /*
     * Vrací objekt čekající na odeslání vzdálenému hráči.
     * Obsahující informaci o proběhlém kole.
     */
    public ObjToSend getObjToSend() {
        return objToSend;
    }

    /*
     * Po každém kole obnovuje informaci zadávanou do GUI Board
     * o právě problěhlém kole.
     */
    public String getTurnToString(int lastX, int lastY, Value value) {
        if (value == Value.FIRST) {
            return "Player #2 on turn. Round #" + getRound() + ". Last action performed on [" + (char) ('A' + lastX) + "," + (char) ('A' + lastY) + "]";
        } else {
            return "Player #1 on turn. Round #" + getRound() + ". Last action performed on [" + (char) ('A' + lastX) + "," + (char) ('A' + lastY) + "]";
        }
    }

    public int getRound() {
        return round;
    }

    public void setTurn(boolean turn) {
        this.turn = turn;
    }

    public void setObjToSend(ObjToSend objToSend) {
        this.objToSend = objToSend;
    }

    /*
     * Metoda testující, zda proběhlé kolo nebylo vítězné.
     * Pro žádné vítězství vrací nulu, jinak vrací konstantu
     * označující hráče (objekt Value).
     */
    public int checkWin(int x, int y) {
        int rtrn = 0;
        for (int i = 0; i < list.length; i++) {
            for (int j = 0; j < list[i].length; j++) {
                Value pom = list[i][j];
                if (pom.getNum() != 0) {
                    try {
                        if (pom.equals(list[i][j + 1])) //doprava horizont
                        {
                            if (pom.equals(list[i][j + 2])) {
                                if (pom.equals(list[i][j + 3])) {
                                    if (pom.equals(list[i][j + 4])) {
                                        return pom.getNum();
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                    }
                    try {
                        if (pom.equals(list[i + 1][j]))//dolu vertikál
                        {
                            if (pom.equals(list[i + 2][j])) {
                                if (pom.equals(list[i + 3][j])) {
                                    if (pom.equals(list[i + 4][j])) {
                                        return pom.getNum();
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                    }
                    try {
                        if (pom.equals(list[i][j - 1])) //doleva horizont
                        {
                            if (pom.equals(list[i][j - 2])) {
                                if (pom.equals(list[i][j - 3])) {
                                    if (pom.equals(list[i][j - 4])) {
                                        return pom.getNum();
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                    }
                    try {
                        if (pom.equals(list[i - 1][j])) //nahoru vertikál
                        {
                            if (pom.equals(list[i - 2][j])) {
                                if (pom.equals(list[i - 3][j])) {
                                    if (pom.equals(list[i - 4][j])) {
                                        return pom.getNum();
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                    }
                    try {
                        if (pom.equals(list[i - 1][j + 1])) // 2
                        {
                            if (pom.equals(list[i - 2][j + 2])) {
                                if (pom.equals(list[i - 3][j + 3])) {
                                    if (pom.equals(list[i - 4][j + 4])) {
                                        return pom.getNum();
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                    }
                    try {
                        if (pom.equals(list[i + 1][j + 1])) //4
                        {
                            if (pom.equals(list[i + 2][j + 2])) {
                                if (pom.equals(list[i + 3][j + 3])) {
                                    if (pom.equals(list[i + 4][j + 4])) {
                                        return pom.getNum();
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                    }
                    try {
                        if (pom.equals(list[i + 1][j - 1])) //7
                        {
                            if (pom.equals(list[i + 2][j - 2])) {
                                if (pom.equals(list[i + 3][j - 3])) {
                                    if (pom.equals(list[i + 4][j - 4])) {
                                        return pom.getNum();
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                    }
                    try {
                        if (pom.equals(list[i - 1][j - 1])) //11
                        {
                            if (pom.equals(list[i - 2][j - 2])) {
                                if (pom.equals(list[i - 3][j - 3])) {
                                    if (pom.equals(list[i - 4][j - 4])) {
                                        return pom.getNum();
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                    }
                }
            }
        }
        return rtrn;
    }

    /*
     * Nastavuje actionListenery pro celé GUI.
     * Zajišťuje i to, že políčka budou pro
     * novou hru neoznačena.
     */
    public final void setListeners() {
        board.exit.addActionListener(new MenuListener());
        board.helpButton.addActionListener(new MenuListener());
        board.newGame.addActionListener(new MenuListener());
        board.hostNewGame.addActionListener(new MenuListener(this));
        board.joinNewGame.addActionListener(new MenuListener(this));
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                board.buttons[i][j].addActionListener(new ButtonListener(i, j));
                board.buttons[i][j].setIcon(null);
            }
        }
    }

    /*
     * Privátní třída zprovozňující políčka GUI. Každý
     * listener uchovává informaci o souřadnicích políčka
     * a po zmáčknutí tyto informace předá metodě performTurn.
     */
    private class ButtonListener implements ActionListener {

        int x;
        int y;

        public ButtonListener(int i, int j) {
            x = i;
            y = j;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (isOnline) {
                if (turn) {
                    if (isServer) {
                        performTurn(x, y, Value.FIRST, true);
                    } else {
                        performTurn(x, y, Value.SECOND, true);
                    }
                } else {
                    board.turnInfo.setText("Cannot play at the moment.");
                }
            } else {
                if (turn) {
                    performTurn(x, y, Value.FIRST, false);
                } else {
                    performTurn(x, y, Value.SECOND, false);
                }
            }
        }
    }

    /*
     * actionListenery obluhující menu GUI. Využívají proměnných
     * třídy Game, aby tyto informace nemusely být předávány konstruktorem,
     * ale byly vyhledány pouze pokud to situace vyžaduje. Pro online hru
     * je znemožněno vytvoření nové hry.
     */
    private class MenuListener implements ActionListener {

        String help = "The player who succeeds in placing five respective marks in a horizontal, vertical, or diagonal row wins the game.\n";
        Game game;

        public MenuListener() {
        }

        public MenuListener(Game game) {
            this.game = game;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getActionCommand().equals("Show Help")) {
                JOptionPane.showMessageDialog(null, help, "Help", JOptionPane.PLAIN_MESSAGE);
            }
            if (e.getActionCommand().equals("New Game")) {
                if (isOnline) {
                    JOptionPane.showMessageDialog(null, "Not supported for online games.", "New game", JOptionPane.PLAIN_MESSAGE);
                } else if (JOptionPane.showConfirmDialog(null, "Are you sure?", "New game", JOptionPane.YES_NO_OPTION) == 0) {
                    board.dispose();
                    offlineAnnul();
                }
            }
            if (e.getActionCommand().equals("Exit")) {
                if (JOptionPane.showConfirmDialog(null, "Are you sure?", "End game", JOptionPane.YES_NO_OPTION) == 0) {
                    System.exit(0);
                }
            }
            if (e.getActionCommand().equals("Host new game")) {
                if (isOnline) {
                    JOptionPane.showMessageDialog(null, "Not supported for online games.", "New game", JOptionPane.PLAIN_MESSAGE);
                } else {
                    for (int i = 0; i < list.length; i++) {
                        for (int j = 0; j < list[i].length; j++) {
                            list[i][j] = Value.NONE;
                        }
                    }
                    isOnline = true;
                    board.setTitle("TicTacToe Player #1");
                    isServer = true;
                    Server server = new Server(isServer, game);
                    setListeners();
                    turn = isServer;
                }
            }
            if (e.getActionCommand().equals("Join new game")) {
                if (isOnline) {
                    JOptionPane.showMessageDialog(null, "Not supported for online games.", "New game", JOptionPane.PLAIN_MESSAGE);
                } else {
                    for (int i = 0; i < list.length; i++) {
                        for (int j = 0; j < list[i].length; j++) {
                            list[i][j] = Value.NONE;
                        }
                    }
                    isOnline = true;
                    board.setTitle("TicTacToe Player #2");
                    isServer = false;
                    Server server = new Server(isServer, game);
                    setListeners();
                    turn = isServer;
                }
            }
        }
    }
}
