import java.io.Serializable;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ondrejtybl
 * 
 * Jednoduchý objekt sloužící pro posílání informací o tahu soupeře při síťové hře.
 * Třída Game obsahuje jeden tento neinicializovaný objekt, který nabude hodnoty
 * v actionListeneru po označení políčka. Vlákno zajišťující síťový přenos
 * pak tím, že objekt není nullový pozná, že má přenášet. Po odeslání znovu vynulluje.
 * Samotná třída Communicator obsahuje další ObjToSend tentokráte pro došasné
 * uložení přijetých souřadnic tahu soupeře.
 * 
 */
public class ObjToSend implements Serializable {
    
    int x;
    int y;

    public ObjToSend(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

}